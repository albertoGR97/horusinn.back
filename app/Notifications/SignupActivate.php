<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Redirect;

class SignupActivate extends Notification
{
    use Queueable;

    private $user;
    private $url;
    private $tipo;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $url, $tipo)
    {
        $this->user = $user;
        $this->url = $url;
        $this->tipo = $tipo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        switch ($this->tipo) {
            case 1:
                $titulo = base64_encode('A un paso');
                $text = base64_encode('Para terminar de activar tu cuenta es necesario descargar la llave, por favor haz clic en el botón de abajo para obtenerla y finalizar.');
                $uri = base64_encode(url('/api/auth/signup/activate/' . $this->user->activation_token));
                $button = base64_encode(1);
                $buttonText = base64_encode('Obtener archivo pem');
                $url = $this->url . '/activation?titulo=' . $titulo . '&text=' . $text . '&uri=' . $uri . '&button=' . $button . '&buttonText=' . $buttonText;
                $subject = 'Confirma tu cuenta';
                $line = 'Se ha creado y aprobado una cuenta de Horus con este correo electrónico, para continuar debes confirmar tu cuenta y obtendrás tu llave de acceso (archivo .pem).';
                $buttonname = 'Confirmar tu cuenta';
                break;
            case 2:
                $titulo = base64_encode('A un paso');
                $text = base64_encode('Para terminar de recuperar tu acceso es necesario descargar la llave, por favor haz clic en el botón de abajo para obtenerla y finalizar.');
                $uri = base64_encode(url('/api/auth/signup/activate/' . $this->user->activation_token));
                $button = base64_encode(1);
                $buttonText = base64_encode('Obtener archivo pem');
                $url = $this->url . '/api/auth/signup/activa/'.$this->user->activation_token;
                $subject = 'Recuperacion de acceso';
                $line = 'Se ha creado y aprobado una recuperacion de acceso de Horus con este correo electrónico, para continuar debes confirmar tu cuenta y obtendrás tu llave de acceso (archivo .pem).';
                $buttonname = 'Recuperar Acceso';
                break;
            }
            return (new MailMessage)
                ->subject($subject)
                ->line($line)
                ->action($buttonname, $url);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
