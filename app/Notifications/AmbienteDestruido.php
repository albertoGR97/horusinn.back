<?php

namespace App\Notifications;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AmbienteDestruido extends Notification
{
    use Queueable;

    private $datos;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($datos = null)
    {
        $this->datos = $datos;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    public function fechaHoy(){
        $dia = date("d");
        $mes = date("m");
        $year = date("Y"); 

        $mes_activo = [
            "01" => 'Enero',
            "02" => 'Febrero',
            "03" => 'Marzo',
            "04" => 'Abril',
            "05" => 'Mayo',
            "06" => 'Junio',
            "07" => 'Julio',
            "08" => 'Agosto',
            "09" => 'Septiembre',
            "10" => 'Octubre',
            "11" => 'Noviembre',
            "12" => 'Diciembre',
        ];

        $fecha = $dia.' de '.$mes_activo[$mes].' del '.$year;
        return $fecha;
    }

    public function toMail($notifiable)
    {
        $producto = $this->datos['producto'];

        $girha = 'Girha, control de asistencias.';
        $cloud = 'BioTime Cloud.';
        $miAsistencia = 'Mi Asistencia.';

        if($producto == $girha || $producto == $cloud || $producto == $miAsistencia){
            setlocale(LC_ALL, "es_MX");
            if($producto == $girha){$fondo="/storage/app/public/fondo_pdf.jpg";}elseif($producto == $cloud){$fondo="/storage/app/public/fondoBTC_pdf.jpg";}else{$fondo="/storage/app/public/fondoMiAsistencia_pdf.jpg";}
            if($producto === $girha){$producto=trim($girha, ".");}elseif($producto === $cloud){$producto=trim($cloud, ".");}else{$producto=trim($miAsistencia, ".");}

            //var_dump($this->datos); die();

            $data = [
                'cliente' => $this->datos['ambiente'],
                'fecha' => AmbienteDestruido::fechaHoy(),
                'imagen' => $fondo,
                'firma' => "/storage/app/public/firma.PNG",
                'descEmpresa' => $this->datos['DS'],
                'producto' => $producto,
                'acceso' => $this->datos['acceso']
            ];

            $pdf = PDF::loadView('mail/ambientes/vista-pdf-destroy', $data)
                ->save(storage_path('app/public/ambientes/') ."Carta de destrucción de ambiente ". $this->datos['ambiente'] . '.pdf');

            return (new MailMessage)
                ->subject('Destrucción de ambiente exitosa')
                ->markdown('mail.ambientes.destruido', $this->datos)
                ->attachData($pdf->output(), "Carta de destrucción de ambiente ". $this->datos['ambiente'] . '.pdf');
        } else {
            return (new MailMessage)
                ->subject('Creación de ambiente exitosa')
                ->markdown('mail.ambientes.creado', $this->datos);
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
