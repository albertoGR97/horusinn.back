<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    use HasFactory;

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $table = 'lectores_clientes';
    protected $primaryKey = 'id';
    public $incrementing = false;


    public static function saveCliente(&$customer)
    {

        $cliente = new Clientes();
        $customer['id_inc']=Clientes::max('id_inc')+1;
        $cliente->id = $customer['id'];
        $cliente->nombre_db = $customer['nombre_db'];
        $cliente->producto = $customer['producto'];
        $cliente->id_inc = $customer['id_inc'];
        $cliente->nombre = $customer['nombre'];
        $cliente->ip_psql = base64_encode(encrypt($customer['ip_psql']));
        return $cliente->save();
    }

    public static function updateCliente($conn, $id_server)
    {
        $conexion = Datos_server::where("id", $conn['id'])->where("server_id",$id_server)->first();
        $conexion->tipo_conexion = base64_encode(encrypt($conn['tipo_conexion']));
        $conexion->puerto = base64_encode(encrypt($conn['puerto']));
        $conexion->usuario = base64_encode(encrypt($conn['usuario']));
        $conexion->password = base64_encode(encrypt($conn['password']));
        $conexion->centraliza = $conn['centraliza'];
        return $conexion->save();
    }

    public static function decodeCliente(&$cliente){
        $cliente->ip_psql = decrypt(base64_decode($cliente->ip_psql));

    }

    public static function deleteCliente($id_inc)
    {
        return Clientes::where("id", $id_inc)->delete();

    }
}
