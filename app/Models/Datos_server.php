<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Datos_server extends Model
{
    use HasFactory;

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $table = 'servers_conexiones';

    protected $primaryKey = 'id';

    public static function getConns($server_id, $tipo_conexion="all")
    {
        $tipo_con_encr = base64_encode(encrypt($tipo_conexion));
        if ($tipo_conexion=='all') {
            $conn = Server::where('server_id', $server_id);

        }
        else if($tipo_conexion=="MySql") {
            $conn = Server::where('server_id', $server_id)->where('tipo_conexion', $tipo_con_encr)
                ->first();
            Server::decodeServer($conn,2);
        }
        else if($tipo_conexion=="PostgreSql") {
            $conn = Server::where('server_id', $server_id)->where('tipo_conexion', $tipo_con_encr)
                ->first();
            Server::decodeServer($conn,2);
        }
        else if($tipo_conexion=="SSH") {
            $conn = Server::where('server_id', $server_id)->where('tipo_conexion', $tipo_con_encr)
                ->first();
            Server::decodeServer($conn,2);
        }



        return $conn;
    }

    public static function saveConns($conexiones_datos, $id_server)
    {
        $fallidos = 0;
        $fallaron = "";

        foreach ($conexiones_datos['servers'] as $datos) {
            $validador = self::saveConn($datos, $id_server);
            if (!$validador) {
                $fallidos++;
                $fallaron .= " " . $datos['tipo_conexion'];
            }
        }

        return [$fallidos, $fallaron];
    }

    public static function saveConn($conn, $id_server)
    {
        $conexion = new Datos_server();
        $conexion->tipo_conexion = base64_encode(encrypt($conn['tipo_conexion']));
        $conexion->puerto = base64_encode(encrypt($conn['puerto']));
        $conexion->usuario = base64_encode(encrypt($conn['usuario']));
        $conexion->dbcentral = !empty($conn['dbcentral']) ? base64_encode(encrypt($conn['dbcentral'])) : null;
        if($conn["tipo_conexion"]=="SSH"){
            if($conn['tipoPassSsh']=='key'){
                $conexion->password = base64_encode(encrypt($conn['private_key']));
            }else{
                $conexion->password = base64_encode(encrypt($conn['password']));
            }
        }else{
            $conexion->password = base64_encode(encrypt($conn['password']));
        }
        //$conexion->tipoPassSsh = $conn['tipoPassSsh'];
        $conexion->producto = !empty($conn['producto']['value']) ? $conn['producto']['value'] : null ;
        $conexion->centraliza = $conn['centraliza'] == 1 || $conn['centraliza'] == true ? 1 : 0;
        $conexion->server_id = $id_server;
        return $conexion->save();
    }

    public static function updateConn($conn, $id_server)
    {

        $conexion = Datos_server::where("id", $conn['id'])->where("server_id",$id_server)->first();
        $conexion->tipo_conexion = base64_encode(encrypt($conn['tipo_conexion']));
        $conexion->puerto = base64_encode(encrypt($conn['puerto']));
        $conexion->usuario = base64_encode(encrypt($conn['usuario']));
        $conexion->dbcentral = base64_encode(encrypt($conn['dbcentral']));
        if($conn["tipo_conexion"]=="SSH"){
            if($conn['tipoPassSsh']=='key'){
                if(strlen($conn['private_key'])>0){
                    $conexion->password = base64_encode(encrypt($conn['private_key']));
                }
            }else{
                $conexion->password = base64_encode(encrypt($conn['password']));
            }
        }else{
            $conexion->password = base64_encode(encrypt($conn['password']));
        }
        $conexion->producto = !empty($conn['producto']['value']) ? $conn['producto']['value'] : !empty($conn['producto']) ? $conn['producto'] : null ;
        $conexion->tipoPassSsh = $conn['tipoPassSsh'];
        $conexion->centraliza = $conn['centraliza'] == 1 || $conn['centraliza'] == true ? 1 : 0;


        return $conexion->save();
    }

    public static function deleteConn($id_conn='',$id_server)
    {
        if(!empty($id_conn)){
            return Datos_server::where("id", $id_conn)->where("server_id", $id_server)->delete();
        }else{
            return Datos_server::where("server_id", $id_server)->delete();
        }

    }
}
