<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class horus_ambiente_log extends Model
{
    use HasFactory;

    public static function f_crearNuevo($datos)
    {
        try {
            $log = new horus_ambiente_log();
            $log->id_server_apli = $datos->server_apli_id;
            $log->id_server_engine  = $datos->server_engine_id;
            $log->id_user = $datos->user_id;
            $log->producto = $datos->producto;
            $log->cliente = $datos->cliente;
            $log->base_ini = now();
            $log->base_fin = now();
            $log->tablas_ini = now();
            $log->tablas_fin = now();
            $log->funciones_ini = now();
            $log->funciones_fin = now();
            $log->eventos_ini = now();
            $log->eventos_fin = now();
            $log->sp_ini = now();
            $log->sp_fin = now();
            $log->triggers_ini = now();
            $log->triggers_fin = now();
            $log->views_ini = now();
            $log->views_fin = now();
            $log->tareas_ini = now();
            $log->tareas_fin = now();
            $log->extras_ini = now();
            $log->extras_fin = now();
            $log->observaciones = 'Nuevo.';
            $log->finished_at = now();
            $status =  $log->save();
            return ['status' => $status, 'id' => $log->id];
        } catch (Exception $e) {
            return ['status' => false, 'id' => $e->getMessage()];
        }
    }

    public static function f_actualizarCampo($datos)
    {
        $log = horus_ambiente_log::find($datos->log_id);
        $campo = $datos->campo;
        if($campo == 'destruir'){
            $datos->campo = 'base';
            $campo = $datos->campo;
        }
        $campo_ini = $datos->campo . '_ini';
        $campo_fin = $datos->campo . '_fin';
        $log->$campo = $datos->status_campo;
        switch ($datos->status_campo) {
            case 0:
                $log->$campo_ini = now();
                $log->$campo_fin = now();
                $log->observaciones = 'En Proceso.';
                break;
            case 1:
                $log->$campo_fin = now();
                break;
            case 2:
                $log->$campo_fin = now();
                $log->observaciones = $datos->error;
                $log->status = 2;
                break;
        }

        if ($datos->campo == 'extras' && $datos->status_campo == 1) {
            $log->observaciones = 'Completado';
            $log->status = 1;
            $log->finished_at = now();
        }
        return $log->save();
    }
}
