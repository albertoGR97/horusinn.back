<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre', 'ip_publica', 'ip_local', 'producto'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'base', 'base_central', 'prefix_base', 'created_at', 'updated_at'
    ];

    public static function decodeServer(&$server, $tipo=2)
    {
        switch ($tipo) {
            case 1:
                $server->ip_publica = decrypt(base64_decode($server->ip));
                $server->ip_local = decrypt(base64_decode($server->ip_local));
                $server->base = decrypt(base64_decode($server->base));
                $server->prefix_base = decrypt(base64_decode($server->prefix_base));
                $server->base_central = decrypt(base64_decode($server->base_central));
                break;
            case 2:
                $server->ip = ($server->ip!=null)? decrypt(base64_decode($server->ip)):null;
                $server->ip_local = ($server->ip!=null)? decrypt(base64_decode($server->ip_local)):null;
                $server->tipo_conexion = ($server->tipo_conexion!=null)? decrypt(base64_decode($server->tipo_conexion)):null;
                $server->puerto = ($server->puerto!=null)? decrypt(base64_decode($server->puerto)):null;
                $server->base = ($server->base!=null)? decrypt(base64_decode($server->base)):null;
                $server->dbcentral = ($server->dbcentral!=null)? decrypt(base64_decode($server->dbcentral)):null;
                $server->prefix_base = ($server->prefix_base!=null)? decrypt(base64_decode($server->prefix_base)):null;
                $server->base_central = ($server->base_central!=null)? decrypt(base64_decode($server->base_central)):null;
                $server->usuario = ($server->usuario!=null)? decrypt(base64_decode($server->usuario)):null;
                $server->password = ($server->password!=null)? decrypt(base64_decode($server->password)):null;
                break;
            case 3:
                $server[0]->ip = ($server[0]->ip!=null)? decrypt(base64_decode($server[0]->ip)):null;
                $server[0]->ip_local = ($server[0]->ip!=null)? decrypt(base64_decode($server[0]->ip_local)):null;
                $server[0]->puerto = ($server[0]->puerto!=null)? decrypt(base64_decode($server[0]->puerto)):null;
                $server[0]->usuario = ($server[0]->usuario!=null)? decrypt(base64_decode($server[0]->usuario)):null;
                $server[0]->password = ($server[0]->password!=null)? decrypt(base64_decode($server[0]->password)):null;
                $server[0]->tipo_conexion = (isset($server[0]->tipo_conexion) && $server[0]->tipo_conexion != null) ? decrypt(base64_decode($server[0]->tipo_conexion)):null;
                break;
        }
    }

    public function scopeExclude($query, $value = [])
    {
        return $query->select(array_diff($this->hidden, (array) $value));
    }



    public static function getServerInfo($serOprod, $centralizadora = 0)
    {
        if ($centralizadora) {
            $server = Server::where('servers.producto', $serOprod)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->where('servers_conexiones.centraliza', 1)
                ->first();
        } else {
            $server = Server::where('nombre', $serOprod)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->where('servers.centraliza', 0)
                ->first();
        }
        Server::decodeServer($server,2);
        return $server;
    }

    public static function getServerbyProduct($product)
    {
        if($product == 'girha' || $product == 'biotime_cloud'){
            $serves = Server::selectRaw('servers.id, nombre, servers.tipo_conexion')->where('servers.producto', $product)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->where('servers_conexiones.centraliza', 0)
            ->get();
        }else{
            $serves = Server::selectRaw('servers.id, nombre, servers.tipo_conexion')->where('servers.producto', $product)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->where('servers.centraliza', 0)
            ->get();

        }

        return $serves;
    }

    public static function getAllInfoServer($id, $nombre){
        $server = Server::selectRaw('servers.id, servers_conexiones.producto as subproducto, servers.producto, ip_local, ip, servers_conexiones.tipo_conexion, servers_conexiones.puerto, servers_conexiones.usuario, servers_conexiones.password')->where('servers.id', $id)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->where('servers.nombre', $nombre)->first();

        Server::decodeServer($server,2);
        return $server;
    }

    public static function saveServer($server)
    {
        self::setBases($server);
        $server_nuevo = Server::where("nombre",$server['nombre'])->firstOrNew();

        $server_nuevo->nombre = $server['nombre'];
        $server_nuevo->producto = $server['producto'];
        $server_nuevo->ip = base64_encode(encrypt($server['ip_publica']));
        $server_nuevo->ip_local = base64_encode(encrypt($server['ip_local']));

        $server_nuevo->puerto = base64_encode(encrypt($server['servers'][0]['puerto']));
        $server_nuevo->usuario = base64_encode(encrypt($server['servers'][0]['usuario']));
        $server_nuevo->password = base64_encode(encrypt($server['servers'][0]['password']));
        $server_nuevo->tipo_conexion = base64_encode(encrypt($server['servers'][0]['tipo_conexion']));

        $server_nuevo->base = base64_encode(encrypt($server['base']));
        $server_nuevo->prefix_base = base64_encode(encrypt($server['prefix_base']));
        $server_nuevo->base_central = base64_encode(encrypt($server['base_central']));

        if ($server_nuevo->save()) {
            return $server_nuevo;
        } else {
            return null;
        }
    }

    public static function updateServer($server_up)
    {
        $server = Server::where('id',  '=', $server_up['id'])->first();
        self::setBases($server_up);

        $server->nombre = $server_up['nombre'];
        $server->producto = $server_up['producto'];
        $server->ip = base64_encode(encrypt($server_up['ip_publica']));
        $server->ip_local = base64_encode(encrypt($server_up['ip_local']));
        $server->base = base64_encode(encrypt($server_up['base']));
        $server->prefix_base = base64_encode(encrypt($server_up['prefix_base']));
        $server->base_central = base64_encode(encrypt($server_up['base_central']));
        if ($server->save()) {
            return $server;
        } else {
            return null;
        }
    }

    public static function setBases(&$server)
    {
        switch ($server['producto']) {
            case "liber":
                $server['base'] = 'liber_11';
                $server['prefix_base'] = 'liber_';
                $server['base_central'] = 'liber';
                break;
            case 'girha':
                $server['base'] = 'girha_default';
                $server['prefix_base'] = 'cliente_';
                $server['base_central'] = 'obelsys';
                break;
            case 'mi_asistencia':
                $server['base'] = 'miasiste_MiAsistencia_0';
                $server['prefix_base'] = 'miasiste_MiAsistencia_';
                $server['base_central'] = 'miasiste_MiAsistencia';
                break;
            case 'biotime_cloud':
                $server['base'] = 'cliente_1218';
                $server['prefix_base'] = 'cliente_';
                $server['base_central'] = 'obelsys';
                break;
        }
    }


    public static function deleteOne($id)
    {
        return  Server::where('id', $id)->delete();
    }
}
