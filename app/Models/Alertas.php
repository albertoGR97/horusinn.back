<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alertas extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre', 'estado','sentencia','hora','correos'
    ];


    public static function updateAlert($alerta)
    {
        $nuevaAlerta =  Alertas::where('id', '=',$alerta['id'])->first();
        $nuevaAlerta->nombre = $alerta['nombre'];
        $nuevaAlerta->estado = $alerta['estado'];
        $nuevaAlerta->hora = $alerta['hora'];
        $nuevaAlerta->correos = $alerta['correos'];

        return $nuevaAlerta->save();
    }

    public static function createAlert($alerta)
    {
        $nuevaAlerta = new Alertas();
        $nuevaAlerta->nombre = $alerta['nombre'];
        $nuevaAlerta->estado = $alerta['estado'];
        $nuevaAlerta->hora = $alerta['hora'];
        $nuevaAlerta->correos = $alerta['correos'];
        if($nuevaAlerta->save()){
            return $nuevaAlerta;
        } else {
            return false;
        }
    }
}
