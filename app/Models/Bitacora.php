<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    use HasFactory;

    protected $fillable = [
        'id', 'id_cliente', 'lectore', 'no_serie', 'estatus', 'problema', 'observacion', 'comentario_cliente'

    ];

    protected $table = 'lectores_bitacora';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'registro', 'created_at', 'updated_at'
    ];

    public static function updateBitacoraByRegistro($registro_up){

        if(isset($registro_up['no_serie'])){
            $bitacora = Bitacora::where('id',  '=', $registro_up['id'])->first();
        }else{
            $bitacora = BitacoraG::where('id',  '=', $registro_up['id'])->first();
        }

        $bitacora->estatus = $registro_up['estatus'];
        $bitacora->problema = $registro_up['problema'];
        $bitacora->observacion = $registro_up['observacion'];
        $bitacora->comentario_cliente = $registro_up['comentario_cliente'];
        $bitacora->actualizo = $registro_up['actualizo'];
        $bitacora->updated_at = $registro_up['updated_at'];

        if ($bitacora->save()) {
            return $bitacora;
        } else {
            return null;
        }
    }
}
