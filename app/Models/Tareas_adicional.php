<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tareas_adicional extends Model
{
    use HasFactory;

    protected $fillable = [
         'nombre', 'tabla', 'producto'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id'

    ];

    public static function getTareasByProduct($produto)
    {
        return  Tareas_adicional::where('producto', $produto)
            ->get();

    }
}
