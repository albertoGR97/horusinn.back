<?php

namespace App\Http\Controllers\log;

use App\Http\Controllers\Controller;
use App\Models\horus_ambiente_log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LogAmbienteController extends Controller
{
    public function crearNuevo(Request $request){
        $mensaje = null;
        $validacion = Validator::make($request->all(),[
            'server_apli_id' => 'required',
            'server_engine_id' => 'required',
            'user_id' => 'required',
            'producto' => 'required',
            'cliente' => 'required'
        ]);
        
        if($validacion->fails()){
            $errores = '';
            foreach($validacion->errors()->all() as $mensajes){
                $errores = $mensajes.', '.$errores;
            }
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'error' => $errores
            ], 401);
        }
        
        $nuevo = horus_ambiente_log::f_crearNuevo($request);
        if($nuevo['status'] == true){
            $mensaje = response()->json([
                'message' => 'log creado',
                'id_log' => $nuevo['id']
            ], 201);
        } else {
            $mensaje = response()->json([
                'message' => 'Error de creacion de log',
                'error' => $nuevo['id']
            ], 500);
        }
        return $mensaje;
    }

    public function actualizarCampo(Request $request)
    {
        $mensaje = null;
        $validacion = Validator::make($request->all(),[
            'status_campo' => 'required|integer',
            'campo' => 'required|string',
            'log_id' => 'required|integer'
        ]);
        if($validacion->fails()){
            $errores = '';
            foreach($validacion->errors()->all() as $mensajes){
                $errores = $mensajes.', '.$errores;
            }
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'error' => $validacion->errors()
            ], 401);
        }

        $campo = horus_ambiente_log::f_actualizarCampo($request);

        if($campo){
            $mensaje = response()->json([
                'message' => $request->campo.' actualizado',
                'status' => $request->status_campo
            ], 201);
        } else {
            $mensaje = response()->json([
                'message' => 'Error al actualizar log',
                'error' => 'No se actualizo el registro.'
            ], 500);
        }
        return $mensaje;
    }

}
