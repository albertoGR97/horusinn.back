<?php

namespace App\Http\Controllers\LectoresCron;

use App\Console\Commands\Ssh2_crontab_manager;
use App\Console\Commands\Ssh2_phpseclib3_contrab_manager;
use App\Http\Controllers\Controller;
use App\Models\Lectores;
use App\Models\Lectores_cron_horas;
use App\Models\LectoresCron;
use Illuminate\Http\Request;
use phpseclib3\Crypt\PublicKeyLoader;
use phpseclib3\Net\SSH2;

class LectoresCronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $crons = LectoresCron::all()->makeHidden(['updated_at', 'created_at'])->toArray();
        echo json_encode($crons);;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $newCron = ($request->toArray());

        $crons = LectoresCron::all()->makeHidden(['updated_at', 'created_at'])->toArray();
        if(!(count($crons)>0)){

                $newCron0 = json_decode(json_encode($newCron), true);
                $newCron = LectoresCron::createLectoresCron($newCron0);

                foreach ($newCron0["horas"] as $hora){
                    if($newCron->estado == 1){
                        $arg_datent_a['actualizar'] = 2;
                        $arg_datent_a['cron_job'] = $hora["sentencia"];
                        $arg_datent_a['activar_alertas'] = $newCron["estado"];
                        $arg_datent_a['tiempo_alertas'] = $hora["hora"];
                        if($newCron->estado == 1){
                            $newCron->sentencia = $this->cronJobRefreshLectores($arg_datent_a);
                        }

                    }
                    $validacion = Lectores_cron_horas::saveCronHora($hora, $newCron->id);

                }



        }
        else{
            return response()->json([
                'message' => 'Favor de editar el cron existente, no se permite crear mas de uno',
                'code' => "g-401",
                'error' => 'Favor de editar el cron existente, no se permite crear mas de uno',
            ], 401);
        }


        $mensaje = "Datos creador correctamente";

        return response()->json($mensaje);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LectoresCron  $lectoresCron
     * @return \Illuminate\Http\Response
     */
    public function show(LectoresCron $lectoresCron)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LectoresCron  $lectoresCron
     * @return \Illuminate\Http\Response
     */
    public function edit(LectoresCron $lectoresCron)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LectoresCron  $lectoresCron
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {

        $cronEdit = ($request->toArray())[0];

            $cronEdit = json_decode(json_encode($cronEdit), true);
            $cron0 = LectoresCron::find($cronEdit['id']);
            LectoresCron::updateLectoresCron($cronEdit);
            if($cronEdit["estado"]!=$cron0["estado"]){
                $horas=Lectores_cron_horas::getCronHoras($cronEdit['id']);
                if($cronEdit["estado"]!=1){
                    foreach ($horas as $hora){
                        $arg_datent_a['cron_job'] = $hora->sentencia;
                        $arg_datent_a['activar_alertas'] = 0;
                        $arg_datent_a['actualizar'] = 0;
                        $arg_datent_a['tiempo_alertas'] = $hora->hora;
                        $hora_actual["sentencia"] = $this->cronJobRefreshLectores($arg_datent_a);
                    }
                }else{
                    foreach ($horas as $hora){
                        $arg_datent_a['cron_job'] = $hora->sentencia;
                        $arg_datent_a['activar_alertas'] = 1;
                        $arg_datent_a['actualizar'] = 0;
                        $arg_datent_a['tiempo_alertas'] = $hora->hora;
                        $hora_actual["sentencia"] = $this->cronJobRefreshLectores($arg_datent_a);
                    }
                }

            }
            $cronEdit = LectoresCron::find($cronEdit['id']);
            $cronEdit->save();


        $mensaje = "Datos actualizados correctamente";

        return response()->json($mensaje,200);
    }


    public function getAllHorasCron(Request $request)
    {
        $conns = Lectores_cron_horas::getCronHoras($request->id);;
        return response()->json($conns);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LectoresCron  $lectoresCron
     * @return \Illuminate\Http\Response
     */
    public function destroy(LectoresCron $lectoresCron)
    {
        //
    }

    public function updateCronHoras(Request $request){
        $server = $request->toArray();
        $cron0 = (LectoresCron::all())[0];
        $val = LectoresCron::updateLectoresCron($server);
        if (!isset($val)) {
            return response()->json([
                'message' => 'Error en el servidor, favor de contactar a soporte.',
                'code' => 500,
                'error' => 'Error al guardar: ' . $server['nombre'],
            ], 500);
        }

        $conexiones = $server['horas'];
        $fallidos = 0;
        $fallo = "";
        $horas_actual=Lectores_cron_horas::where("id_lectores_cron",$server['id'])->get();
        $bandera=false;
        if(count($horas_actual)>0){
            foreach ($horas_actual as $hora_actual){
                $bandera=false;
                foreach ($server['horas'] as $horaEdit) {
                    $horaEdit['id']=$horaEdit['id_table'];
                    if($hora_actual['id']==$horaEdit['id']){
                        $bandera = true;
                        break;
                    }
                }
                if($bandera==false){
                    $arg_datent_a['cron_job'] = $hora_actual->sentencia;
                    $arg_datent_a['activar_alertas'] = 0;
                    $arg_datent_a['actualizar'] = 0;
                    $arg_datent_a['tiempo_alertas'] = $hora_actual->hora;
                    $hora_actual["sentencia"] = $this->cronJobRefreshLectores($arg_datent_a);
                    Lectores_cron_horas::deleteCronHora($hora_actual['id'],$hora_actual['id_lectores_cron']);
                }
            }
        }
        foreach ($server['horas'] as $hora_edit) {
            $hora_edit['id']=$hora_edit['id_table'];
            $hora_actual = Lectores_cron_horas::find($hora_edit['id_table']);

        /*    $arg_datent_a['actualizar'] = 1;
            $arg_datent_a['cron_job'] = $hora_edit["sentencia"];
            $arg_datent_a['activar_alertas'] = $server['estado'] ;
            $arg_datent_a['tiempo_alertas'] = $hora_edit["hora"];

            $hora_edit["sentencia"] = $this->cronJobRefreshLectores($arg_datent_a);*/

            if(isset($hora_actual)){
                if ($hora_actual->hora != $hora_edit["hora"]) {
                    $arg_datent_a['actualizar'] = 1;
                } else {
                    $arg_datent_a['actualizar'] = 0;
                }
                $arg_datent_a['cron_job'] = $hora_edit["sentencia"];
                $arg_datent_a['activar_alertas'] = $server['estado'] ;
                $arg_datent_a['tiempo_alertas'] = $hora_edit["hora"];
                if($arg_datent_a['actualizar'] ==1||$server['estado']!=$cron0['estado']){
                    $hora_edit["sentencia"] = $this->cronJobRefreshLectores($arg_datent_a);
                }
                $validacion = Lectores_cron_horas::updateCronHora($hora_edit, $server['id']);
            }else{
                $arg_datent_a['actualizar'] = 2;
                $arg_datent_a['cron_job'] = $hora_edit['sentencia'];
                $arg_datent_a['activar_alertas'] = $server['estado'] ;
                $arg_datent_a['tiempo_alertas'] = $hora_edit['hora'];
                if($server['estado'] == 1){
                    $hora_edit['sentencia'] = $this->cronJobRefreshLectores($arg_datent_a);
                }
                $validacion = Lectores_cron_horas::saveCronHora($hora_edit, $server['id']);
            }
        }
        if (!isset($validacion)) {
            return response()->json([
                'message' => 'Error en el servidor, favor de contactar a soporte.',
                'code' => 500,
                'error' => 'Error al guardar: ' . $server['nombre'],
            ], 500);
        }

        return response()->json([
            'message' => "Servidor(es) Actualizado(s)",
            'code' => 200
        ], 200);
    }

    public function ejecutaRefresh()
    {
        Lectores::f_ejecutaGetClientes();
    }


    private function cronJobRefreshLectores($arg_datent_a)
    {

        $priv = "";
        $pub = "";
        $pass="";


        $priv=
            'PuTTY-User-Key-File-2: ssh-rsa
            Encryption: none
            Comment: jorge.gomez@girha.com.mx
            Public-Lines: 6
            AAAAB3NzaC1yc2EAAAABJQAAAQEAlpdtQW+azBU5XVK2an9K4Y3l21KYF2cKYPd4
            svgC00mCTbUdis57YljLPC75CFxiS+GJr/lHLW3r2eB5ImnjK5o6oDniIr4gvF/n
            /zLzD1orLne19kjKalUgtlTkCbacTxLz+bkbW3QsauJLl1jM3mjlZ50E6ONW2rtz
            8Q1booKeUk/r1IwlLul+aEBOOnFkt/cV9Om0xA0+bI1XPFXeOAt3y+1A0XwKFec6
            h4VNDyEf3IitSMwttgAg/s4AtXYeMe1fQu68BcQm8cicHXmRz4rjgtmvHsQzNIcP
            jKd6UK2CoQunnSBZcbMNZMFHkHXSvwubN5FT8AAu8OrN9gQBsQ==
            Private-Lines: 14
            AAABAH4r2BQ66XqOUqgitIK9rXDeq9Nutsd/2ENnjqPI38XH94024WZ1paVs/U4Z
            hIp26pKaXppbO6KaXdI4nNek0xaxpr2YR9frZ4kSFWcc2XuQvGUz3aTVM/g5fEW8
            vw8OnqMdt6e2w+TW9MhBDuaWgiIgiNpFSU2ANANeyOyOpriVqjKmNJ+/ToK+usF1
            JfmEWVceBo59QeKYjsMjI80WWLAqLW/9oWklwmAdLpLgs6TLoPhiuMvj9c6ouL/9
            o4JrpzT/po/wecxzipxtPjO6gOis6nDMove82wh2TvUQpqUJz678GGVVQQG+GuKB
            jGCzaY+++mP9gPvlpq32r4oGzZkAAACBAMox5DoYs/P6jPluV1yZiCD+XLZ/LcPi
            /ZfwR48qUzi1v6No8SXrk+XdomyQP6gas4/iNL5NaCliCtEUGonBs8iIAfLXnpXt
            JOPrEzD4PwS4hlofngwTOL4rdkfaGtBjobB3Udmd1Vi/HbbP7ohvpp2WkjLxb0SF
            N4z27OHe62+vAAAAgQC+qily7KmJLPS+2TPU0fB1/wsuWA2qgAb8F37v664rI6Xg
            OCg5NtwbMOgrBebfDrRT+yhtQdgQflU5mjInaJyvIFcyAZQ54oj4bQm9zrbJ8MrM
            aP0SFWf7/vcvptOg9oQ9YSGP2O2Bj3ih2Q7VhBoaQxmYQmcaoTqpfPBD3bWcnwAA
            AIEAx+twtwQp8Z72uiXh54UzBE34eFg56Dzz4gmBmyCEChMwya09gugOrMLnuPI1
            IlaI4GFf4aKJHKmwLJ8M2XNrf/aitSOMqIOs1aZC43Lz2v74/iYFqaXXJuoaquOq
            MwR4TJTSNIXGHQ7GAMRrRrIpJxXZq0tZy/CSAgZ/curU2lk=
            Private-MAC: c35fab1c53d28c1bf22541c31c5ec43996b4e679';

        $privXD = '1930100%Ja';

        //$crontab = new Ssh2_crontab_manager('localhost', '22', 'zanzakigus', 'Sasukecry1');
        //$crontab = new Ssh2_phpseclib3_contrab_manager('34.70.146.156', '22', 'jorge.gomez', $priv);
        //$crontab = new Ssh2_crontab_manager('34.70.146.156', '22', 'soporte.operativo', '', "/var/www/html/horus/servidor/publickey", "/var/www/html/horus/servidor/private_key.pem");
        //$crontab = new Ssh2_crontab_manager('13.67.178.43', '22', 'girha-admincall', 'Db_Girh@*246');
        $crontab = new Ssh2_crontab_manager('localhost', 22, 'hp', '1930100%Ja', $priv);
        //$crontab = new Ssh2_crontab_manager('localhost', 22, 'gdesarrollo', 'Girha*12345');


        if ($arg_datent_a['actualizar'] == 1 && $arg_datent_a['activar_alertas'] == 1) {
            $crontab->remove_cronjob($arg_datent_a['cron_job']);
        }



        if ($arg_datent_a['activar_alertas'] == 1) {


            $hora = explode(":", date('G:i', strtotime($arg_datent_a['tiempo_alertas'])));
            if (str_split($hora[1])[0] > 0) {
                $minutos = $hora[1];
            } else {
                $minutos = str_split($hora[1])[1];
            }

            $fecha_hora_ejecucion = $minutos . ' ' . $hora[0] . ' * * *';

            $job = $fecha_hora_ejecucion . ' curl ' . url('/api/auth/lectoresCron/ejecutaRefresh');

            $crontab->append_cronjob($job);
            return $job;
        } else {
            $crontab->remove_cronjob($arg_datent_a['cron_job']);
            return $arg_datent_a['cron_job'];
        }
    }



}
