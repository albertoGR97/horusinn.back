<?php

namespace App\Http\Controllers\Server;


use App\Http\Controllers\Controller;
use App\Models\Datos_server;
use App\Models\Server;
use Exception;
use Illuminate\Http\Request;
use PDO;
use stdClass;
use Illuminate\Support\Facades\DB;
use phpseclib3\Crypt\PublicKeyLoader;
use phpseclib3\Net\SSH2;

class ServerController extends Controller
{


    public function index()
    {
        $serves = Server::all();
        foreach ($serves as $server) {
            Server::decodeServer($server, 1);
        }
        return response()->json($serves);
    }

    public function obtenerBD(Request $request)
    {
        $server = $request->toArray();
        $subproducto = isset($server['servers'][0]["producto"]) ? $server['servers'][0]["producto"]["value"] : '';

        if(isset($request['req']) && $request['req'] == 1){
            $svr = $server;
            $conns = Server::getAllInfoServer($request['id'], $request['nombre']);
            $server = $conns;
            $subproducto = !empty($svr['subproducto']) ? $svr['subproducto'] : $server['subproducto'];
        }

        $prueba = $this->testConnectionMySql($server);
        if ($prueba[0] > 0) {
            return response()->json([
                'message' => 'Datos inválidos',
                'code' => 500,
                'error' => 'Para poder marcar la opción "Centralizado" debe llenar los datos correctamente',
            ], 500);
        }

        $server['ip_publica'] = isset($server['ip']) ? $server['ip'] : $server['ip_publica'];
        $dato['puerto'] = !isset($server['servers'][0]['puerto']) ? $server['puerto'] : $server['servers'][0]['puerto'];
        $dato['usuario'] = !isset($server['servers'][0]['usuario']) ? $server['usuario'] : $server['servers'][0]['usuario'];
        $dato['password'] = !isset($server['servers'][0]['password']) ? $server['password'] : $server['servers'][0]['password'];

        $con = new PDO('mysql:host=' . $server['ip_publica'] . '; port=' . $dato['puerto'] . '; dbname=mysql;', $dato['usuario'], $dato['password']);

        switch (isset($request->producto) ? $request->producto : $server->producto) {
            case 'girha':
                $resp = $con->query("SELECT schema_name as id FROM information_schema.schemata WHERE schema_name != 'information_schema' && schema_name != 'mysql' && schema_name != 'sys' && schema_name != 'performance_schema'");
                //$bases = DB::select('select id_cliente as id from obelsys.clientes where tipo_conexion = 1');
                break;
            case 'mi_asistencia':
                if($subproducto == 'mi_asistencia'){
                    $resp = $con->query("SELECT schema_name as id FROM information_schema.schemata WHERE schema_name != 'information_schema' && schema_name != 'mysql' && schema_name != 'sys' && schema_name != 'performance_schema'");
                    //$bases = DB::select('select ID_INDEX as id from miasiste_MiAsistencia.EMPRESAS');
                }elseif($subproducto == 'cloudclock'){
                    $resp = $con->query("SELECT schema_name as id FROM information_schema.schemata WHERE schema_name != 'information_schema' && schema_name != 'mysql' && schema_name != 'sys' && schema_name != 'performance_schema'");
                    //$bases = DB::select('select ID_INDEX as id from cloudclo_CloudClock.EMPRESAS');
                }elseif($subproducto == 'mi_asistencia_hik'){
                    $resp = $con->query("SELECT schema_name as id FROM information_schema.schemata WHERE schema_name != 'information_schema' && schema_name != 'mysql' && schema_name != 'sys' && schema_name != 'performance_schema'");
                    //$bases = DB::select('select ID_INDEX as id from asishik_MiAsistenciaByHik.EMPRESAS');
                }else{
                    return response()->json([
                        'message' => 'Error interno',
                        'code' => 500,
                        'error' => 'Se ha producido un inconveniente',
                    ], 500);
                }
                break;
            case 'liber':
                $resp = $con->query("SELECT schema_name as id FROM information_schema.schemata WHERE schema_name LIKE '%cliente_%'");
                break;
            case 'biotime_cloud':
                $resp = $con->query("SELECT schema_name as id FROM information_schema.schemata WHERE schema_name != 'information_schema' && schema_name != 'mysql' && schema_name != 'sys' && schema_name != 'performance_schema'");
                //$bases = DB::select('select id_cliente as id from obelsys.clientes where tipo_conexion = 2');
                break;
        }

        $resp->execute();
        $respuestas = $resp->fetchAll();

        return response()->json($respuestas);
    }

    public function getConnsByIdServer(Request $request)
    {
        $conns = Datos_server::where("server_id", $request->id)->get();
        foreach ($conns as $data) {
            Server::decodeServer($data, 2);
        }
        return response()->json($conns);
    }

    public function store(Request $request)
    {
        $server = $request->toArray();
        $prueba = $this->testConnection($server);
        if ($prueba[0] > 0) {
            return response()->json([
                'message' => 'Error de conexión',
                'code' => 500,
                'error' => 'Conexión no establecida con:' . $prueba[1],
            ], 500);
        }

        $info_server = Server::saveServer($server);
        if ($info_server === false) {
            return response()->json([
                'message' => "Ocurrio un error, favor de contactar a soporte",
                'code' => 500,
                'error' => "Error al guardar los datos pricipales"
            ], 500);
        }

        $conns = Datos_server::saveConns($server, $info_server->id);
        if ($conns[0] > 0) {
            $mensaje = [
                'message' => "Error al guardar el o los servidores",
                'code' => 500,
                'error' => "Fallaron: " . $conns[1] . "."
            ];
            $codigo = 500;
        } else {
            $mensaje = [
                'message' => "Servidor agregado",
                'code' => 200,
            ];
            $codigo = 200;
        }
        return response()->json($mensaje, $codigo);
    }

    /**
     * Update the specified resource in storage.
     */
    public function updateServers(Request $request)
    {
        $servers = $request->toArray();
        $fallo = "";
        $fallaron = 0;

        foreach ($servers as $server) {
            $conns = Datos_server::where("server_id", $server['id'])->get();
            foreach ($conns as $conn) {
                Server::decodeServer($conn, 2);
            }
            $server['servers'] = json_decode(json_encode($conns), true);
            $prueba = $this->testConnection($server);

            if ($prueba[0] > 0) {
                $fallaron++;
                $fallo .= $server['nombre'] . " ";
            }
        }
        if ($fallaron > 0) {
            return response()->json([
                'message' => 'Error de conexión',
                'code' => 500,
                'error' => 'Conexiónes no establecidas con:' . $fallo,
            ], 500);
        }
        $fallo = "";
        $fallaron = 0;
        foreach ($servers as $server) {
            $val = Server::updateServer($server);
            if (!isset($val)) {
                $fallaron++;
                $fallo .= $server['nombre'] . " ";
            }
        }
        if ($fallaron > 0) {
            return response()->json([
                'message' => 'Error en el servidor, favor de contactar a soporte.',
                'code' => 500,
                'error' => 'Error al guardar: ' . $fallo,
            ], 500);
        }

        return response()->json([
            'message' => "Servidor(es) Actualizado(s)",
            'code' => 200
        ], 200);
    }

    public function updateConnsServer(Request $request)
    {
        $server = $request->toArray();
        $prueba = $this->testConnection($server);
        if ($prueba[0] > 0) {
            return response()->json([
                'message' => 'Error de conexión',
                'code' => 500,
                'error' => 'Conexiónes no establecidas con:' . $prueba[1]
            ], 500);
        }

        $val = Server::updateServer($server);
        if (!isset($val)) {
            return response()->json([
                'message' => 'Error en el servidor, favor de contactar a soporte.',
                'code' => 500,
                'error' => 'Error al guardar: ' . $server['nombre'],
            ], 500);
        }

        $conexiones = $server['servers'];
        $fallidos = 0;
        $fallo = "";
        foreach ($server['acciones'] as $accion) {
            switch ($accion['accion']) {
                case "C":
                    $conexion = $conexiones[$this->searchConnForId($accion['id_conn'], $conexiones)];
                    if(isset($conexion)){
                        $validacion = Datos_server::saveConn($conexion, $val->id);
                        if(!$validacion){
                            $fallidos++;
                            $fallo .= $conexion['tipo_conexion']." ";
                        }
                    }
                    break;
                case "U":
                    $conexion = $conexiones[$this->searchConnForId($accion['id_conn'], $conexiones)];
                    if(isset($conexion)){
                        $validacion = Datos_server::updateConn($conexion, $val->id);
                        if(!$validacion){
                            $fallidos++;
                            $fallo .= $conexion['tipo_conexion']." ";
                        }
                    }
                    break;
                case "D":
                    $validacion = Datos_server::deleteConn($accion['id_conn'], $val->id);
                    break;
            }
        }


        return response()->json([
            'message' => "Servidor(es) Actualizado(s)",
            'code' => 200
        ], 200);
    }



    public function getServerbyProduct(Request $request)
    {
        $request->validate([
            'producto'   => 'required|string',
        ]);

        $server = Server::getServerbyProduct($request->producto)->toArray();
        $server = $this->fullDecrypt($server);

        return json_encode($server);
    }

    public function searchConnForId($conn, $array)
    {
        foreach ($array as $key => $val) {
            if ($val['tipo_conexion'] === $conn) {
                return $key;
            }
        }
        return null;
    }

    public function deleteOne($id)
    {
        Server::deleteOne($id);
        Datos_server::deleteConn('', $id);

        $mensaje = "Datos eliminados correctamente.";

        return response()->json($mensaje);
    }

    public function deletegeneral(Request $request)
    {
        $id = $request->toArray();
        $total = count($id);

        for ($i = 0; $i < $total; $i++) {

            Server::deleteOne($request[$i]);
        }

        $mensaje = "Datos eliminados correctamente.";

        return response()->json($mensaje);
    }

    private function testConnection($datos = null)
    {
        $cont = 0;
        $servers = '';
        foreach ($datos['servers'] as $dato) {
            if ($dato['tipo_conexion'] == 'MySql') {
                try {
                    $conn = new PDO('mysql:host=' . $datos['ip_publica'] . '; port=' . $dato['puerto'] . '; dbname=mysql;', $dato['usuario'], $dato['password']);
                    $conn = null;
                } catch (Exception $pdoe) {
                    $conn = null;
                    $cont++;
                    $servers .= $dato['tipo_conexion'] . " ";
                }
            } else if ($dato['tipo_conexion'] == 'PostgreSql') {
                try {
                    $conn = new PDO('pgsql:host=' . $datos['ip_publica'] . '; port=' . $dato['puerto'] . '; dbname=postgres;', $dato['usuario'], $dato['password']);
                    $conn = null;
                } catch (Exception $pdoe) {
                    $conn = null;
                    $cont++;
                    $servers .= $dato['tipo_conexion'] . " ";
                }
            } else if ($dato['tipo_conexion'] == 'SSH') {
                try {
                    if($dato['tipoPassSsh']=="key"){
                        $key = PublicKeyLoader::load($dato['private_key']);

                        $ssh = new SSH2($datos['ip_publica']);
                        if (!$ssh->login($dato['usuario'], $key)) {
                            $ssh->disconnect();
                            $cont++;
                            $servers .= $dato['tipo_conexion'] . " ";
                        }else{
                            $ssh->disconnect();
                        }
                    }else{
                        $conexion = ssh2_connect($datos['ip_publica'], $dato['puerto']);
                        ssh2_auth_password($conexion, $dato['usuario'], $dato['password']);
                        ssh2_disconnect($conexion);
                        }

                } catch (Exception $e) {
                    ssh2_disconnect($conexion);
                    $cont++;
                    $servers .= $dato['tipo_conexion'] . " ";
                }
            } else {
                $cont++;
                $servers .= $dato['tipo_conexion'] . " ";
            }
        }
        return [$cont, $servers];
    }

    private function testConnectionMySql($datos = null){
        $cont = 0;
        $servers = '';
        $datos['ip_publica'] = isset($datos['ip']) ? $datos['ip'] : $datos['ip_publica'];
        $dato['puerto'] = !isset($datos['servers'][0]['puerto']) ? $datos['puerto'] : $datos['servers'][0]['puerto'];
        $dato['usuario'] = !isset($datos['servers'][0]['usuario']) ? $datos['usuario'] : $datos['servers'][0]['usuario'];
        $dato['password'] = !isset($datos['servers'][0]['password']) ? $datos['password'] : $datos['servers'][0]['password'];
        try {
            $conn = new PDO('mysql:host=' . $datos['ip_publica'] . '; port=' . $dato['puerto'] . '; dbname=mysql;', $dato['usuario'], $dato['password']);
            $conn = null;
        } catch (Exception $pdoe) {
            $conn = null;
            $cont++;
        }
        return [$cont];
    }

    //desencriptado
    private function fullDecrypt($serves){
        foreach ($serves as  $key => $serve) {
            foreach ($serve as $columName => $valor) {
                if ($columName != 'nombre' and $columName != 'id' and $columName != 'producto' and $columName != 'centraliza' and $columName != 'server_id') {
                    $serves[$key][$columName] = decrypt(base64_decode($valor));
                }
            }
        }
        return $serves;
    }



    function prueba()
    {
        $server = Server::getServerInfo('server_1');
    }
}
