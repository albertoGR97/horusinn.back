<?php

namespace App\Http\Controllers\Pdf;

use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class PdfController extends Controller
{
    public function crearNuevo(Request $request)
    {
        setlocale(LC_ALL, "es_MX");
        $fecha = strftime("%d de %h del %Y");
        $data = [
            'password' => $request->password,
            'usuario' => $request->usuario,
            'cliente' => $request->cliente,
            'descEmpresa' => $request->descEmpresa,
            'contacto' => $request->contacto,
            'fecha' => $fecha,
            'imagen' => "/storage/app/public/fondo_pdf.jpg",
            'firma' => "/storage/app/public/firma.PNG"
        ];

        $pdf = PDF::loadView('mail/ambientes/vista-pdf', $data)
            ->save(storage_path('app/public/') . 'archivo.pdf');

        return response()->json(['creado'], 200);
    }
}
