<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewUserRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $datos;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($datos)
    {
        $this->datos = $datos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $opt = [

            'actionUrl1'            => $this->datos['acceptA'],
            'actionUrl2'            => $this->datos['acceptU'],
            'actionUrl3'            => $this->datos['decline'],
            'correo'                => $this->datos['emailAccount'],
            'nombre'                => $this->datos['nombreTo'],
            'displayableActionUrl'  => str_replace(['mailto:', 'tel:'], '', 'jsdj'),
            'linea1' => $this->datos['linea1'],
            'linea2' => $this->datos['linea2'],
            'acceptTextA' => $this->datos['acceptAText'],
            'acceptTextU' => $this->datos['acceptUText'],
            'declineText' => $this->datos['declineText'],
            'asunto' => $this->datos['asunto'],
            'activo'=> 1,
        ];
        return $this->subject($this->datos['asunto'])
                    ->markdown('mail.auth.pend', $opt);
    }
}
