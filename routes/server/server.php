<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'namespace' => 'App\Http\Controllers\Server',
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('server/index', 'ServerController@index');
        Route::post('server/dataCentraliza', 'ServerController@obtenerBD');
        Route::post('server/getConnsByIdServer', 'ServerController@getConnsByIdServer');
        Route::post('server/save', 'ServerController@store');
        Route::post('server/delete/{name}', 'ServerController@deleteOne');
        Route::post('server/deletegeneral', 'ServerController@deletegeneral');
        Route::post('server/updateServers', 'ServerController@updateServers');
        Route::post('server/updateConnsServer', 'ServerController@updateConnsServer');
        Route::post('server/getServerbyProduct', 'ServerController@getServerbyProduct');

    });

    Route::get('server/pruebas', 'ServerController@prueba');
});
