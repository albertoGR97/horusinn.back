<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'namespace' => 'App\Http\Controllers\Alertas',
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {

    Route::post('alertas/ejecutaAlerta', 'AlertasController@ejecutaAlerta');
    Route::post('alertas/validarRespaldo', 'AlertasController@validarRespaldo');

    Route::get('alertas/validarServer', 'AlertasController@validarServer');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('alertas/index', 'AlertasController@index');
        Route::post('alertas/save', 'AlertasController@store');
        Route::post('alertas/delete/{name}', 'AlertasController@deleteOne');
        Route::post('alertas/deletegeneral', 'AlertasController@deletegeneral');
        Route::post('alertas/update', 'AlertasController@update');
        Route::post('alertas/getServerbyProduct', 'AlertasController@getServerbyProduct');

    });
});
