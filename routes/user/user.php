<?php

use App\Models\MailAux;
use App\Models\User;
use App\Notifications\SignupAuthorization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group([
    'namespace' => 'App\Http\Controllers\User',
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {





    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('users', 'UserController@allUsers');
        Route::post('users/delete', 'UserController@deleteUser');
        Route::post('users/edit', 'UserController@editUser');

    });
});
