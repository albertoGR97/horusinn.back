<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'namespace' => 'App\Http\Controllers\Lectores',
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::get('lectores/index', 'LectoresController@index');
    Route::post('lectores/lectoresbycliente', 'LectoresController@getLectoresByCliente');
    Route::post('lectores/getbitacoraBySerie', 'LectoresController@getbitacoraBySerie');
    Route::post('lectores/savebitacoraBySerie', 'LectoresController@savebitacoraBySerie');
    Route::post('lectores/updateBitacora', 'LectoresController@updateBitacora');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
    });

});