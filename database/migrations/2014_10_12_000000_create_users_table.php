<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
/*            $table->text('login_usuario',50)->unique();*/
            $table->string('nombre',200);
            $table->string('email',100)->unique();
            $table->text('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('activation_token',200);
            $table->text('public_key');
            $table->text('private_key')->nullable();
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
