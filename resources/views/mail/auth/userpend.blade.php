@component('mail::message')
# @lang('Hola!')
<br>

<p>Su cuenta fue creada pero necesita ser validada por un administrador.</p>
<p>Por favor, espere el siguiente correo.</p>

@lang('Saludos'),<br>
{{ config('app.name') }}


@endcomponent
